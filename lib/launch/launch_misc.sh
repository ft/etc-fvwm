#!/bin/sh

cd "${FVWM_LAUNCH_WD}"

exec ${FVWM_USERDIR}/bin/fvwm_terminal.sh tmux attach-session -t misc
