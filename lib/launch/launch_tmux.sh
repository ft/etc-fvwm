#!/bin/sh

cd "${FVWM_LAUNCH_WD}"

exec ${FVWM_USERDIR}/bin/fvwm_terminal.sh ${FVWM_USERDIR}/bin/tmux_new_or_attach.sh "$1"
