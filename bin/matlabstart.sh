#!/bin/sh

workdir="${HOME}/lib/matlab"

[ -d "${workdir}" ] && cd "${workdir}"
echo "Starting Matlab, please stand by..."
/usr/local/matlab/bin/matlab > ~/var/log/matlab.log 2>&1
