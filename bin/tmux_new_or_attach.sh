#!/bin/sh

session="$1"
shift

## Redirection disabled for now, because it seems to make
## `has-session' block forever with non existing sessions.
#tmux has-session -t "${session}" > /dev/null 2>&1 && exists=yes || exists=no
tmux has-session -t "${session}" && exists=yes || exists=no

if [ "${exists}" = 'yes' ] ; then
    set -- "$@" attach-session -t "${session}"
else
    set -- "$@" new-session -s "${session}"
fi

exec tmux "$@"
