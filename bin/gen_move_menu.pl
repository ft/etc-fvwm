#! /usr/bin/perl
### vim:ft=perl:fdm=marker
###
### create move to desktop menu
###
### @@webhtml_contact@@
### Last-Modified: @@webhtml_last_modified@@
###
### <@@webhtml_uri@@>
###

use strict;
my ($desktop, $menu, $fd, $ld, $cd, $hexnum);

if ($#ARGV != 3) {
  print "usage: gen_move_menu.pl <menuname> <firstdesktop> <lastdesktop> <currentdesktop>\n";
  exit(1);
}

$menu=$ARGV[0];
$fd=$ARGV[1];
$ld=$ARGV[2];
$cd=$ARGV[3];

#print "$menu\n$fd\n$ld\n$cd\n";

for $desktop ($fd .. $ld) {
  $hexnum = sprintf "%lx", $desktop;
  if ($desktop != $cd) {
    print "AddToMenu $menu \"0x&" . uc($hexnum) . "\" MoveWinToDesktop $desktop\n";
  } else {
    print "AddToMenu $menu \"  -- 0x" . uc($hexnum) . " --  \"\n";
  }
}
