#!/usr/bin/perl
use strict;
my ($command, $ch, $line, $sname, $attached, $i, %sessions, @mn);
my $menu=$ARGV[0];
my $exec=$ARGV[1];
my $arg=$ARGV[2];

$command = "tmux list-sessions";

sub is_attached {
    my ($line) = @_;
    return 1 if ($line =~ m/\(attached\)$/);
    return 0;
}

if ($menu eq 'menutmux') {
    open $ch, q{-|}, $command or die "Running tmux failed: $!\n";
    while ($line = <$ch>) {
        chomp $line;
        ($sname) = $line =~ m/^(\S+):/;
        $attached = is_attached($line);
        if ($sname =~ m/^(misc|email|web)$/) {
            print "AddToMenu $menu \"&$sname";
            if ($attached) {
                print " (attached)" if ($attached);
            }
            print "\" Exec exec $exec $arg $sname\n";
        }
    }
    close $ch;
    print "+ \"\"  Nop\n";
    print "+ \"&attach/new\" Module FvwmForm TmuxSessionDialog\n";
    print "+ \"\"  Nop\n";
    print "+ \"e&xisting\" Popup dynsessions\n";
    exit 0;
}

@mn = qw{ 0 1 2 3 4 5 6 7 8 9 a b c d e f g h i j k l m n o p q r s t u v w x y z };

open $ch, q{-|}, $command or die "Running tmux failed: $!\n";
while ($line = <$ch>) {
    chomp $line;
    ($sname) = $line =~ m/^(\S+):/;
    $attached = is_attached($line);
    if ($sname !~ m/^(misc|email|web)$/) {
        $sessions{$sname}  = "AddToMenu $menu \"[\@mn\@] $sname";
        $sessions{$sname} .= " (attached)" if ($attached);
        $sessions{$sname} .= "\" Exec exec $exec $arg $sname\n";
    }
}
close $ch;

$i = 0;
foreach my $key (sort keys %sessions) {
    $sessions{$key} =~ s/\@mn\@/&$mn[$i++]/;
    print $sessions{$key};
}
exit 0;
