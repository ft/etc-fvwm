#!/bin/sh
###
### is a certain screen session running?
###
### @@webhtml_contact@@
### Last-Modified: @@webhtml_last_modified@@
###
### <@@webhtml_uri@@>
###

if [ -z "$1" ] ; then
    printf 'usage: screen_running.sh <sessionname>\n'
    exit 1
fi

screen_name="$1"
running=''

screen -ls |    \
    sed         \
        -e '/^[a-zA-Z0-9]/d'    \
        -e '/^\r*$/d'           \
        -e 's/^[ \t]*[0-9][0-9]*\.\([^ \t]*\)[ \t].*/\1/' \
| while IFS= read -r line; do
    if [ "${line}" = "${screen_name}" ] ; then
        printf '1'
        return
    fi
done

printf '0'
exit 0
