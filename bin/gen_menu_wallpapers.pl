#!/usr/bin/perl
### vim:ft=perl:fdm=marker
###
### dynamic wallpaper generator for fvwm
###   takes three parameters:
###     'wallpaper directory'
###     'menu-name'
###     'picture-suffix'
###     'program'
###
### Naming for wallpapers:
###     catergorie-number-name.${picture-suffix}
###
### To give you an idea what i mean:
###
### % ls -1
### cars-000-audiA4.png
### cars-001-audiRSQ.png
### cars-002-autoB4cabrio.png
### cars-004-generalsmokie.png
### cars-005-mustang.png
### cars-006-generallee.png
### cars-007-charger.png
### computer-000-help.png
### computer-001-hack.png
### female-000-db.png
### misc-000-butterfly.png
### misc-001-hackse.png
### misc-002-dontgetcaught.png
### misc-003-cannabis.png
### nature-000-cloudylake.png
### nature-001-darklake.png
### nature-002-colorfulllake.png
### tech-000-submarine.png
### tech-001-helicopter.png
###
### @@webhtml_contact@@
### Last-Modified: @@webhtml_last_modified@@
###
### <@@webhtml_uri@@>
###

my $menu  = $ARGV[1];
my $dir   = $ARGV[0];
my $suff  = $ARGV[2];
my $prog  = $ARGV[3];
my $i;
opendir(DIR, $dir) or die "couldn't open \"$dir\"\n";
my @pics = sort grep { /\.$suff$/ && -f "$dir/$_" } readdir(DIR);
closedir(DIR);
my @string;
my $key;
my %cat;

for $i (0..$#pics) {
  if ($pics[$i] =~ m/([a-zA-Z0-9]*)-([0-9]*)-([a-zA-Z0-9]*)\.png/g) {
    $cat{$1}{"$2. $3"} = $pics[$i];
  }
  elsif ($pics[$i] =~ m/([a-zA-Z0-9]*)-([0-9]*)\.png/g) {
    $cat{$1}{"pic $2"} = $pics[$i];
  }
  else {
    $cat{unsorted}{$pics[$i]} = $pics[$i];
  }
}

foreach $key (sort keys %cat) {
  my $ikey;
  print   "DestroyMenu  recreate $menu-$key\n";
  print   "AddToMenu    $menu-$key     \"$key\"  Title\n";
  foreach $ikey (sort keys %{ $cat{$key} }) {
    $sc_ikey=$ikey;
    $sc_ikey=~s/(00)([0-9])(.*)/$1&$2$3/;
    print "AddToMenu    $menu-$key     \"\%$dir/thumbs/$cat{$key}{$ikey}\%$sc_ikey\"         Exec exec $prog \"$dir/$cat{$key}{$ikey}\"\n";
    #print "AddToMenu    $menu-$key     \"$ikey\"         Exec exec $prog \"$dir/$cat{$key}{$ikey}\"\n";
  }
}

foreach $key (sort keys %cat) {
  print "AddToMenu $menu \"$key\" PopUp $menu-$key\n";
}

