#!/usr/bin/perl
###
### load "standard" applications
###
### @@webhtml_contact@@
### Last-Modified: @@webhtml_last_modified@@
###
### <@@webhtml_uri@@>
###

use warnings;
use strict;

my $cfgdir=$ENV{'FVWM_USERDIR'} . "/etc";
my ($cfgh, $i, %commands);

$i=1;

open $cfgh, q{<}, "$cfgdir/std_programs.cfg"
    or die "Could not open $cfgdir/std_programs.cfg: $!\n";

while (my $line = <$cfgh>) {
    my ($desk, $command);

    chomp $line;
    next if ($line =~ m/^\s*$/ || $line =~ m/^\s*#/);

    if (!(($desk, $command) = $line =~ m/^\s*(\d+)\s+(.*)$/)) {
        die "Error parsing $cfgdir/std_programs.cfg (line: $i)!\n";
    }
    push @{ $commands{$desk} }, $command;
    $i++;
}

close $cfgh;

sub fvwm_run {
    my ($desk,$command) = @_;

    #print "(echo 'GotoDesk 0 $desk'; echo 'Exec $command') | FvwmCommand -c -r\n";
    system "(echo 'GotoDesk 0 $desk'; echo 'Exec $command') | FvwmCommand -c -r";

    # what a gross hack... :-/
    # I can't find another way to do this.
    sleep(2);
}

foreach my $desk (sort keys %commands) {
    foreach my $command (@{ $commands{$desk} }) {
        fvwm_run($desk, $command);
    }
}
