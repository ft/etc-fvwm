#!/bin/sh

#xlock -mode blank +usefirst -enablesaver -resetsaver -dpmsstandby 180 -dpmssuspend 300 -dpmsoff 600 -background grey65 &
lox -1 "- $(hostname): screen locked (hibernate) -" -2 "" -3 "password: " -1l 30 -1y 10 -3l 100 -3y 50 -4l 25 -s '..' -nu -se &
sleep 1
if [ -z "$1" ] ; then
    sudo /usr/sbin/hibernate
else
    sudo /usr/sbin/hibernate-"$1"
fi
