#!/bin/sh
set -x
LC_CTYPE="${FVWM_LAUNCH_CTYPE}"
export LC_CTYPE
exec "${FVWM_LAUNCH_TERMINAL}"          \
        -name "${FVWM_LAUNCH_NAME}"     \
        -title "${FVWM_LAUNCH_TITLE}"   \
        -e "$@"
