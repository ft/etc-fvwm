#!/bin/sh
###
### smart launch an application
###
### @@webhtml_contact@@
### Last-Modified: @@webhtml_last_modified@@
###
### <@@webhtml_uri@@>
###

if [ -z "$1" ] ; then
    printf 'usage: fvwm_launch.sh <application>\n'
    exit 1
fi

app="$1"
shift
cfg="${FVWM_USERDIR}/etc/launch_${app}.cfg"
globalcfg="${FVWM_USERDIR}/etc/fvwm_launch.cfg"
script="${FVWM_USERDIR}/lib/launch/launch_${app}."

found=0
for ext in sh pl py rb zsh ksh bash csh ; do
    if [ -r "${script}${ext}" ] ; then
        script="${script}${ext}"

        if [ ! -x "${script}" ] ; then
            printf 'Launch script found, but not executable!\n'
            printf 'Script file: %s\n' "${script}"
            exit 1
        fi

        found=1
        break
    fi
done

if [ "${found}" -eq 0 ] ; then
    printf 'No launch script found for %s.\n' "${app}"
    printf 'Script directory: %s\n' "${FVWM_USERDIR}/lib/launch/"
    exit 1
fi

if [ -r "${globalcfg}" ] ; then
    printf 'Loading  : %s\n' "${globalcfg}"
    . "${globalcfg}"
fi
if [ -r "${cfg}" ] ; then
    printf 'Loading  : %s\n' "${cfg}"
    . "${cfg}"
fi

FVWM_LAUNCH_WD=${FVWM_LAUNCH_WD:-$HOME}
FVWM_LAUNCH_CTYPE=${FVWM_LAUNCH_CTYPE:-$LC_CTYPE}
FVWM_LAUNCH_TERMINAL=${FVWM_LAUNCH_TERMINAL:-xterm}
FVWM_LAUNCH_TITLE=${FVWM_LAUNCH_TITLE:-$app}
FVWM_LAUNCH_NAME=${FVWM_LAUNCH_NAME:-$app}

export FVWM_LAUNCH_WD
export FVWM_LAUNCH_CTYPE
export FVWM_LAUNCH_TERMINAL
export FVWM_LAUNCH_TITLE
export FVWM_LAUNCH_NAME

printf 'Executing: %s\n' "${script}"
exec "${script}" "$@"
